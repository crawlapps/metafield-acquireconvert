<?php

namespace App;

use App\Model\MetafieldConfiguration;
use Illuminate\Database\Eloquent\Model;

class GroupableMetafield extends Model
{
    public function configuration(){
        return $this->belongsTo(MetafieldConfiguration::class,'metafield_configuration_id','id');
    }
}
