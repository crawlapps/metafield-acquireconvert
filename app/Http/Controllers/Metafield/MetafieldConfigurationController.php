<?php

namespace App\Http\Controllers\Metafield;

use App\GroupableMetafield;
use App\Http\Controllers\Controller;
use App\Http\Requests\MetafieldConfigurationRequest;
use App\MetafieldGroup;
use App\Model\MetafieldConfiguration;
use App\Model\Setting;
use App\Model\Group;
use Illuminate\Http\Request;
use MongoDB\Driver\Exception\ExecutionTimeoutException;
use OhMyBrew\ShopifyApp\Facades\ShopifyApp;
use Response;

class MetafieldConfigurationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->has('api')) {
            return $this->json($request);
        }
    }

    public function json($request)
    {
        try {
            $shop = \ShopifyApp::shop();
            if ($request->api == '1') {
                $entities = MetafieldConfiguration::where('resource_type', $request->resourceType)->where('shop_id', $shop->id)->where('group_id', null)->orderBy('sort_order', 'asc')->get();
            } else {
                $entities = MetafieldConfiguration::where('shop_id', $shop->id)->where('resource_type', 'NOT LIKE', 'lcl_%')->where('resource_type', 'NOT LIKE', 'sync_%')->where('group_id', null)->orderBy('sort_order', 'asc')->get();
            }
            $entity = [];
            if ($entities) {
                foreach ($entities as $k => $e) {
                    $entity[$k]['id'] = $e->id;
                    $entity[$k]['namespace'] = $e->namespace;
                    $entity[$k]['key'] = $e->key;
                    $entity[$k]['type'] = $e->type;
                    $entity[$k]['label'] = $e->label;
                    $entity[$k]['rtype'] = $e->resource_type;
                    $entity[$k]['group_id'] = $e->group_id;
                }
            }

            $namespace = Setting::where('key', 'global_namespace')->where('shop_id', $shop->id)->first();
            $groups = Group::select('id', 'title', 'description', 'resource_type AS rtype')->where('shop_id', $shop->id)->with('value')->get();
            // $spareGroups = [];
            // if($groups){
            //     foreach($groups as $group){
            //         $grp = [];
            //         $grp['id'] = $group->id;
            //         $grp['title'] = $group->title;
            //         $grp['description'] = $group->description;
            //     }
            // }

            $data['namespace'] = ($namespace) ? $namespace->value : '';
            $data['groups'] = $groups;
            $data['entity'] = $entity;
            return response::json(['data' => $data], 200);
        } catch (\Exception $e) {
            return response::json(['data' => $e->getMessage()], 422);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MetafieldConfigurationRequest $request)
    {
        try {
            if ($request->isSubmit) {

                $shop = \ShopifyApp::shop();
                $data = $request->data;

                foreach ($data as $dk => $dv) {
                    if ($dv['id']) {
                        $metafield = MetafieldConfiguration::where('id', $dv['id'])->first();
                    } else {
                        $metafield = new MetafieldConfiguration;
                    }
                    $metafield->shop_id = $shop->id;
                    $metafield->label = $dv['label'];
                    $metafield->namespace = $dv['namespace'];
                    $metafield->key = $dv['key'];
                    $metafield->type = $dv['typev'];
                    $metafield->sort_order = $dk;
                    $metafield->resource_type = $request->resourceType;
                    $metafield->save();
                }

                if ($remove = $request->removeField) {
                    foreach ($remove as $rk => $rv) {
                        MetafieldConfiguration::where('id', $rv)->delete();
                    }
                }
                return response::json(['data' => 'Metafield Saved!'], 200);
            } else {
                return response::json(['data' => 1], 200);
            }
        } catch (Exception $e) {
            return response::json(['data' => 'Metafield not added...' . $e->getContent()], 422);
        }
    }

    public function allMetafieldConfiguration(MetafieldConfigurationRequest $request)
    {
        try {
            if ($request->isSubmit) {
                $shop = \ShopifyApp::shop();
                $data = $request->data;

                foreach ($data as $key => $val) {
                    if ($val) {
                        if (isset($val['value'])) {
                            foreach ($val['value'] as $k => $v) {
                                if ($val['value'][$k]['id']) {
                                    $metafield = MetafieldConfiguration::where('id', $v['id'])->where(
                                        'shop_id',
                                        $shop->id
                                    )->first();
                                    if (!$metafield) {
                                        $metafield = new MetafieldConfiguration;
                                    }
                                } else {
                                    $metafield = new MetafieldConfiguration;
                                }
                                $metafield->shop_id = $shop->id;
                                $metafield->label = $v['label'];
                                $metafield->namespace = $v['namespace'];
                                $metafield->key = $v['key'];
                                $metafield->type = $v['typev'];
                                $metafield->sort_order = $k;
                                $metafield->resource_type = $v['rtype'];
                                if ($metafield->group_id == null) {
                                    $metafield->save();
                                }
                            }
                        }

                        if (isset($val['groups'])) {
                            // dd($val['groups']);
                            foreach ($val['groups'] as $gk => $gv) {

                                // Saving Configurations Group
                                if ($val['groups'][$gk]['id']) {
                                    $group = Group::where('id', $val['groups'][$gk]['id'])->where(
                                        'shop_id',
                                        $shop->id
                                    )->first();
                                    if (!$group) {
                                        $group = new Group;
                                    }
                                } else {
                                    $group = new Group;
                                }
                                $group->shop_id = $shop->id;
                                $group->title = $gv['title'];
                                $group->description = $gv['description'];
                                $group->resource_type = $val['rtype'];
                                $group->save();

                                $MetafieldGroups = MetafieldGroup::where('group_id', $group->id)->count();
                                if ($MetafieldGroups < 1) {
                                    $MetafieldGroup = new MetafieldGroup();
                                    $MetafieldGroup->shop_id = $shop->id;
                                    $MetafieldGroup->group_id = $group->id;
                                    $MetafieldGroup->resource_type = $val['rtype'];
                                    $MetafieldGroup->save();
                                }

                                // Saving Configurations
                                if ($gv['value']) {
                                    foreach ($gv['value'] as $gi => $gval) {
                                        $new = false;
                                        if ($gval['id']) {
                                            $metafield = MetafieldConfiguration::where('id', $gval['id'])->where(
                                                'shop_id',
                                                $shop->id
                                            )->first();
                                            if (!$metafield) {
                                                $new = true;
                                                $metafield = new MetafieldConfiguration;
                                            } else {
                                                if ($metafield->type != $gval['typev']) {
                                                    $groupables = GroupableMetafield::where('metafield_configuration_id', $metafield->id)
                                                        ->get();
                                                    foreach ($groupables as $groupable) {
                                                        $groupable->metafield_type = $gval['typev'];
                                                        $groupable->save();
                                                    }
                                                }
                                            }
                                        } else {
                                            $new = true;
                                            $metafield = new MetafieldConfiguration;
                                        }
                                        $metafield->shop_id = $shop->id;
                                        $metafield->label = $gval['label'];
                                        $metafield->namespace = $gval['namespace'];
                                        $metafield->key = $gval['key'];

                                        $metafield->type = $gval['typev'];
                                        $metafield->sort_order = $gk;
                                        $metafield->group_id = $group->id;
                                        $metafield->resource_type = $gval['rtype'];
                                        if ($metafield->group_id > 0) {
                                            $metafield->save();

                                            if ($new) {
                                                $metafieldGroups = MetafieldGroup::where('group_id', $group->id)->get();
                                                foreach ($metafieldGroups as $metafieldGroup) {
                                                    $groupable = new GroupableMetafield();
                                                    $groupable->shop_id = $shop->id;
                                                    $groupable->metafield_configuration_id = $metafield->id;
                                                    $groupable->metafield_group_id = $metafieldGroup->id;
                                                    $groupable->value = '';
                                                    $groupable->resource_type =  $gval['rtype'];
                                                    $groupable->metafield_type = $gval['typev'];
                                                    $groupable->owner_id = $metafieldGroup->owner_id;
                                                    $groupable->save();
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                if ($remove = $request->removeField) {
                    foreach ($remove as $rk => $rv) {
                        MetafieldConfiguration::where('id', $rv)->delete();
                    }
                }
                if ($remove = $request->removeGroupIds) {
                    foreach ($remove as $rk => $rv) {
                        $all = MetafieldConfiguration::where('group_id', $rv)->get('id');
                        if ($all) {
                            MetafieldConfiguration::destroy($all);
                        }
                        Group::where('id', $rv)->delete();
                    }
                }
                return response::json(['data' => 'Metafield Saved!'], 200);
            } else {
                return response::json(['data' => 1], 200);
            }
        } catch (Exception $e) {
            return response::json(['data' => 'Metafield not added...' . $e->getContent()], 422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
