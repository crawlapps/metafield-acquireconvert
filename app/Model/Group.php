<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    public $fillable = ['*'];

    public function value()
    {
        return $this->hasMany(MetafieldConfiguration::class, 'group_id', 'id')
            ->select('id', 'group_id', 'key', 'label', 'namespace', 'resource_type AS rtype', 'type AS typev', 'type AS typen', 'sort_order');
        // ->pluck('typen');
    }

    public function configurations()
    {
        return $this->hasMany(MetafieldConfiguration::class, 'group_id', 'id')
            ->select('id', 'group_id', 'key', 'label', 'namespace', 'resource_type', 'type','sort_order');
    }
}
