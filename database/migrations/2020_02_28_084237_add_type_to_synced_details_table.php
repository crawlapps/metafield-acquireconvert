<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTypeToSyncedDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('synced_details', function (Blueprint $table) {
            $table->string('type')->nullable()->comment('type like which job is runing (e.g: syncing, export, import)')->after('description');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('synced_details', function (Blueprint $table) {
            //
        });
    }
}
