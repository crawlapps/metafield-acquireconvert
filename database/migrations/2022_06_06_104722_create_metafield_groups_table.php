<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMetafieldGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('metafield_groups', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('shop_id')->unsigned();
            $table->unsignedBigInteger('group_id')->nullable()->comment('Group id of metafield configuration');
            $table->json('metafield_json')->nullable();
            $table->string('handle')->nullable();
            $table->string('resource_type')->nullable();
            $table->string('metafield_id')->nullable();
            $table->string('owner_id')->nullable();

            $table->foreign('shop_id')->references('id')->on('shops')->onDelete('CASCADE');
            $table->foreign('group_id')->references('id')->on('groups')->onDelete('CASCADE');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('metafield_groups');
    }
}
