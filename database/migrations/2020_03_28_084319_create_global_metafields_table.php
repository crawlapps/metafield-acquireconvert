<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGlobalMetafieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('global_metafields', function (Blueprint $table) {
            $table->char('id',36);
            $table->primary(['id']);
            $table->integer('shop_id')->unsigned();
            $table->char('metafield_configuration_id',36)->nullable();
            $table->string('add_in')->nullable();
            $table->string('titles')->nullable();
            $table->longText('value')->nullable();
            $table->timestamps();

            $table->foreign('shop_id')->references('id')->on('shops')->onDelete('CASCADE');
            $table->foreign('metafield_configuration_id')->references('id')->on('metafield_configurations')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('global_metafields');
    }
}
