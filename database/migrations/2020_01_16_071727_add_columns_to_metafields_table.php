<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToMetafieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('metafields', function (Blueprint $table) {
            $table->string('resource_type')->nullable()->after('metafield_type')->comment('ex: shop,product.. for any perticular metafield');
            $table->char('owner_id',36)->nullable()->after('resource_type')->comment('id of any perticular resource type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('metafields', function (Blueprint $table) {
            //
        });
    }
}
