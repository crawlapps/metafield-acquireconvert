<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGroupableMetafieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('groupable_metafields', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('shop_id')->unsigned();
            $table->unsignedBigInteger('metafield_group_id')->nullable();
            $table->longText('value')->nullable();
            $table->char('metafield_configuration_id', 36)->nullable();
            
            $table->string('metafield_type')->nullable();
            $table->string('resource_type')->nullable();
            $table->char('owner_id', 36)->nullable();

            $table->foreign('shop_id')->references('id')->on('shops')->onDelete('CASCADE');
            $table->foreign('metafield_group_id')->references('id')->on('metafield_groups')->onDelete('CASCADE');
            $table->foreign('metafield_configuration_id')->references('id')->on('metafield_configurations')->onDelete('CASCADE');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('groupable_metafields');
    }
}
